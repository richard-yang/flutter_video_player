import Flutter
import AVFAudio
import UIKit

public class VideoPlayerPlugin: NSObject, FlutterPlugin {
    
    var videoPlayers: [Int64: VideoPlayer];
    var registry: FlutterTextureRegistry;
    static var messenger : FlutterBinaryMessenger? = nil;
    
    public static func register(with registrar: FlutterPluginRegistrar) {
        let _messenger = registrar.messenger();
        messenger = _messenger;
        let channel = FlutterMethodChannel(name: "video_player", binaryMessenger: _messenger)
        let instance = VideoPlayerPlugin(textureRegistry: registrar.textures())
        registrar.addMethodCallDelegate(instance, channel: channel);
    }
    
    init(textureRegistry: FlutterTextureRegistry) {
        self.registry = textureRegistry;
        self.videoPlayers = [:];
        super.init();
    }
    
    public func detachFromEngine(for registrar: FlutterPluginRegistrar) {
        disposeAllPlayers();
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        let methodName = call.method;
        let args = call.arguments as? [String : Any];
        
        switch methodName {
        case "video.init":
            initialize();
            result(nil);
            
        case "video.create":
            let mixWithOthers = args?["mixWithOthers"] as? Bool;
            if (mixWithOthers != nil) {
                self.setMixWithOthers(mixWithOthers: mixWithOthers!);
            }
            let allowBgPlayback = args?["allowBackgroundPlayback"] as? Bool;
            if (allowBgPlayback != nil) {
                // TODO:
            }
            let texId = create();
            let player = self.videoPlayers[texId];
            if (player != nil) {
                let info = [
                    "textureId": texId,
                    "sharedTextureId": player!.renderer.getTextureId()
                ] as [String : Any];
                result(info);
            } else {
                print("Failed to create video player");
                result(FlutterError(code: "Failed to create video player", message: nil, details: nil));
            }
            
        default:
            if (args != nil) {
                handlePlayerOp(opName: methodName, opArgs: args!, result: result);
            } else {
                print("Must specify arguments for video player operations");
                result(FlutterError(code: "Must specify arguments for video player operations", message: nil, details: nil));
            }
        }
    }
    
    private func handlePlayerOp(opName: String, opArgs: [String: Any], result: @escaping FlutterResult) {
        let texId = opArgs["textureId"] as? Int64;
        if (texId == nil) {
            result(FlutterError(code: "Video player ID must be specified", message: nil, details: nil));
            return;
        }
        let player = self.videoPlayers[texId!];
        if (player == nil) {
            result(FlutterError(code: "Cannot find specified player", message: nil, details: nil));
            return;
        }
        switch opName {
        case "video.player.sharedTextureId":
            result(player!.getSharedTextureId());
        case "video.player.setDataSource":
            let dataSrcDesc = opArgs["dataSource"] as? [String: Any];
            if (dataSrcDesc != nil) {
                let uri = dataSrcDesc!["uri"] as! String;
                player!.setDataSource(URL.init(string: uri)!, httpHeaders: nil);
                result(nil);
            } else {
                result(FlutterError(code: "No source specified", message: nil, details: nil));
            }
        case "video.player.play":
            player!.play();
            result(nil);
        case "video.player.pause":
            player!.pause();
            result(nil);
        case "video.player.setLooping":
            player!.setIsLooping(isLooping: opArgs["looping"] as! Bool);
            result(nil);
        case "video.player.setVolume":
            let vol = opArgs["volume"] as! NSNumber;
            player!.setVolume(volume: vol.floatValue);
            result(nil);
        case "video.player.setPlaybackSpeed":
            let spd = opArgs["speed"] as! NSNumber;
            player!.setPlaybackSpeed(speed: spd.floatValue);
            result(nil);
        case "video.player.seekToPos":
            player!.seekTo(location: opArgs["position"] as! Int64);
            result(nil);
        case "video.player.getPos":
            result(player!.position());
        case "video.player.getAbsolutePos":
            result(player!.position());
        case "video.player.setMixWithOthers":
            self.setMixWithOthers(mixWithOthers: opArgs["mixWithOthers"] as! Bool);
            result(nil);
        case "video.player.dispose":
            self.registry.unregisterTexture(texId!);
            self.videoPlayers.removeValue(forKey: texId!);
            // If the Flutter contains https://github.com/flutter/engine/pull/12695,
            // the `player` is disposed via `onTextureUnregistered` at the right time.
            // Without https://github.com/flutter/engine/pull/12695, there is no guarantee that the
            // texture has completed the un-reregistration. It may leads a crash if we dispose the
            // `player` before the texture is unregistered. We add a dispatch_after hack to make sure the
            // texture is unregistered before we dispose the `player`.
            //
            // TODO(cyanglaz): Remove this dispatch block when
            // https://github.com/flutter/flutter/commit/8159a9906095efc9af8b223f5e232cb63542ad0b is in
            // stable And update the min flutter version of the plugin to the stable version.
            let deadline = DispatchTime.now() + .seconds(1);
            DispatchQueue.main.asyncAfter(deadline: deadline) {
                if (!player!.disposed) {
                    player!.dispose();
                }
            }
            result(nil);
        default:
            result(FlutterError(code: "Unimplemented operation", message: opName, details: nil));
        }
    }
    
    private func disposeAllPlayers() {
        for player in self.videoPlayers {
            self.registry.unregisterTexture(player.key);
            player.value.dispose();
        }
        self.videoPlayers.removeAll();
    }
    
    func initialize() {
        do {
            // Allow audio playback when the Ring/Silent switch is set to silent
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback);
        } catch _ {
            print("Failed to allow audio playback when silent switch is set");
        }
        disposeAllPlayers();
    }
    
    func create() -> Int64 {
        let frameUpdater = FrameUpdater(registry: self.registry);
        let player = VideoPlayer().initWithUpdater(frameUpdater);

        let textureId = self.registry.register(player.renderer);
        frameUpdater.textureId = textureId;
        let eventChannel = FlutterEventChannel(name: "videoPlayer/videoEvents" + String(textureId), binaryMessenger: VideoPlayerPlugin.messenger!);
        eventChannel.setStreamHandler(player);
        player.eventChannel = eventChannel;
        self.videoPlayers[textureId] = player;

        print("Created video player and registered texture \(textureId) registry=\(self.registry)");

        return textureId;
    }
    
    func setMixWithOthers(mixWithOthers: Bool) {
        do {
            if (mixWithOthers) {
                try AVAudioSession.sharedInstance().setCategory(
                    AVAudioSession.Category.playback, options: AVAudioSession.CategoryOptions.mixWithOthers);
            } else {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback);
            }
        } catch _ {
            print("Failed to set mixWithOthers");
        }
    }
}
