import AVFoundation
import Flutter
import GLKit

public class FrameUpdater: NSObject {
    var textureId: Int64;
    var registry: FlutterTextureRegistry;
    var renderer: VideoRender?;
    
    init(registry: FlutterTextureRegistry) {
        self.registry = registry;
        self.textureId = -1;

        super.init();

        print("FrameUpdater registry=\(self.registry)");
    }
    
    public func setTextureId(textureId: Int64) {
        self.textureId = textureId;
    }

    public func setRenderer(_ renderer: VideoRender) {
        self.renderer = renderer;
    }
    
    @objc public func updateFrame() {
        self.registry.textureFrameAvailable(self.textureId);
        DispatchQueue.main.async {
            //print("FrameUpdater thread=\(Thread.current)");
            self.renderer!.updateVideoTexture();
        }
    }
}

public class VideoPlayer: NSObject, FlutterStreamHandler {
    
    var renderer: VideoRender;
    var player: AVPlayer?;
    // This is to fix 2 bugs: 1. blank video for encrypted video streams on iOS 16
    // (https://github.com/flutter/flutter/issues/111457) and 2. swapped width and height for some video
    // streams (not just iOS 16).  (https://github.com/flutter/flutter/issues/109116).
    // An invisible AVPlayerLayer is used to overwrite the protection of pixel buffers in those streams
    // for issue #1, and restore the correct width and height for issue #2.
    var playerLayer: AVPlayerLayer?;
    var displayLink: CADisplayLink?;
    var eventSink: FlutterEventSink?;
    var eventChannel: FlutterEventChannel?;
    var frameUpdater: FrameUpdater?;
    var preferredTransform: CGAffineTransform?;
    var disposed: Bool = false;
    var isPlaying: Bool = false;
    var isLooping: Bool = false;
    var isInitialized: Bool = false;
    
    override init() {
        self.renderer = VideoRender(0);

        self.player = AVPlayer();
        self.player!.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none;
        // This is to fix 2 bugs: 1. blank video for encrypted video streams on iOS 16
        // (https://github.com/flutter/flutter/issues/111457) and 2. swapped width and height for some
        // video streams (not just iOS 16).  (https://github.com/flutter/flutter/issues/109116). An
        // invisible AVPlayerLayer is used to overwrite the protection of pixel buffers in those streams
        // for issue #1, and restore the correct width and height for issue #2.
        self.playerLayer = AVPlayerLayer(player: self.player);

        super.init();

        print("VideoPlayer ctor finish");
    }
    
    public func initWithUpdater(_ frameUpdater: FrameUpdater) -> VideoPlayer {
        self.frameUpdater = frameUpdater;
        self.frameUpdater?.setRenderer(self.renderer);
        rootViewController().view.layer.addSublayer(self.playerLayer!);
        return self;
    }
    
    @discardableResult
    public func setDataSource(_ url: URL, httpHeaders: Dictionary<String, String>?) -> VideoPlayer {
        print("Set data source: \(String(describing: url))");
        var options: Dictionary<String, Any>? = nil;
        if (httpHeaders != nil) {
            options = ["AVURLAssetHTTPHeaderFieldsKey": httpHeaders!];
        }
        let urlAsset = AVURLAsset(url: url, options: options);
        let playerItem = AVPlayerItem(asset: urlAsset);
        return initWithPlayerItem(playerItem);
    }
    
    /*
     public func initWithAsset(_ asset: String, frameUpdater: FrameUpdater) {
     let mainBundle = Bundle();
     let path = mainBundle.path(forResource: asset, ofType: nil);
     return initWithURL(URL(fileURLWithPath: path), frameUpdater: frameUpdater, httpHeaders: nil);
     }
     */
    
    func initWithPlayerItem(_ playerItem: AVPlayerItem) -> VideoPlayer {
        self.player?.replaceCurrentItem(with: playerItem);
        
        let asset = playerItem.asset;
        func assetCompletionHandler() {
            if (asset.statusOfValue(forKey: "tracks", error: nil) == AVKeyValueStatus.loaded) {
                let tracks = asset.tracks(withMediaType: AVMediaType.video);
                if (tracks.count > 0) {
                    let videoTrack = tracks[0];
                    func trackCompletionHandler() {
                        if (self.disposed) {
                            return;
                        }
                        if (videoTrack.statusOfValue(forKey: "preferredTransform", error: nil) == AVKeyValueStatus.loaded) {
                            // Rotate the video by using a videoComposition and the preferredTransform
                            self.preferredTransform = getStandardizedTransformForTrack(videoTrack);
                            // Note:
                            // https://developer.apple.com/documentation/avfoundation/avplayeritem/1388818-videocomposition
                            // Video composition can only be used with file-based media and is not supported for
                            // use with media served using HTTP Live Streaming.
                            playerItem.videoComposition =
                                getVideoCompositionWithTransform(self.preferredTransform!, asset: asset, videoTrack: videoTrack);
                        }
                    }
                    videoTrack.loadValuesAsynchronously(forKeys: ["preferredTransform"], completionHandler: trackCompletionHandler);
                }
            }
        }

        createVideoOutputAndDisplayLink(self.frameUpdater!);
        
        addObservers(playerItem);
        asset.loadValuesAsynchronously(forKeys: ["tracks"], completionHandler: assetCompletionHandler);
        
        return self;
    }
    
    func getStandardizedTransformForTrack(_ track: AVAssetTrack) -> CGAffineTransform {
      var t = track.preferredTransform;
      let size = track.naturalSize;
      // Each case of control flows corresponds to a specific
      // `UIImageOrientation`, with 8 cases in total.
      if (t.a == 1 && t.b == 0 && t.c == 0 && t.d == 1) {
        // UIImageOrientationUp
        t.tx = 0;
        t.ty = 0;
      } else if (t.a == -1 && t.b == 0 && t.c == 0 && t.d == -1) {
        // UIImageOrientationDown
        t.tx = size.width;
        t.ty = size.height;
      } else if (t.a == 0 && t.b == -1 && t.c == 1 && t.d == 0) {
        // UIImageOrientationLeft
        t.tx = 0;
        t.ty = size.width;
      } else if (t.a == 0 && t.b == 1 && t.c == -1 && t.d == 0) {
        // UIImageOrientationRight
        t.tx = size.height;
        t.ty = 0;
      } else if (t.a == -1 && t.b == 0 && t.c == 0 && t.d == 1) {
        // UIImageOrientationUpMirrored
        t.tx = size.width;
        t.ty = 0;
      } else if (t.a == 1 && t.b == 0 && t.c == 0 && t.d == -1) {
        // UIImageOrientationDownMirrored
        t.tx = 0;
        t.ty = size.height;
      } else if (t.a == 0 && t.b == -1 && t.c == -1 && t.d == 0) {
        // UIImageOrientationLeftMirrored
        t.tx = size.height;
        t.ty = size.width;
      } else if (t.a == 0 && t.b == 1 && t.c == 1 && t.d == 0) {
        // UIImageOrientationRightMirrored
        t.tx = 0;
        t.ty = 0;
      }
      return t;
    }
    
    func addObservers(_ playerItem: AVPlayerItem) {
        print("Add observers for player item: \(String(describing: playerItem))");

        playerItem.addObserver(self, forKeyPath: "loadedTimeRanges", options: [.initial, .new], context: nil);
        playerItem.addObserver(self, forKeyPath: "status", options: [.initial, .new], context: nil);
        playerItem.addObserver(self, forKeyPath: "presentationSize", options: [.initial, .new], context: nil);
        playerItem.addObserver(self, forKeyPath: "duration", options: [.initial, .new], context: nil);
        playerItem.addObserver(self, forKeyPath: "playbackLikelyToKeepUp", options: [.initial, .new], context: nil);
        playerItem.addObserver(self, forKeyPath: "playbackBufferEmpty", options: [.initial, .new], context: nil);
        playerItem.addObserver(self, forKeyPath: "playbackBufferFull", options: [.initial, .new], context: nil);
        
        // Add an observer that will respond to itemDidPlayToEndTime
        NotificationCenter.default.addObserver(self, selector: #selector(itemDidPlayToEndTime), name: .AVPlayerItemDidPlayToEndTime, object: playerItem);
    }
    
    @objc func itemDidPlayToEndTime(notification: Notification) {
        if (self.isLooping) {
            let playerItem: AVPlayerItem = notification.object as! AVPlayerItem;
            playerItem.seek(to: CMTime.zero, completionHandler: nil);
        } else if (self.eventSink != nil) {
            self.eventSink?(["event": "completed"]);
        }
    }
    
    public override func observeValue(forKeyPath keyPath: String?,
                                      of object: Any?,
                                      change: [NSKeyValueChangeKey : Any]?,
                                      context: UnsafeMutableRawPointer?) {
        if (object == nil) { return; }
        if (keyPath == "loadedTimeRanges") {
            if (self.eventSink != nil) {
                let item = object! as! AVPlayerItem;
                let values = NSMutableArray();
                for timeRange in item.loadedTimeRanges {
                    let range = timeRange.timeRangeValue;
                    let start = CMTimeToMillis(range.start);
                    values.add([start, start + CMTimeToMillis(range.duration)]);
                }
                self.eventSink?(["event": "bufferingUpdate", "values": values]);
            }
        } else if (keyPath == "status") {
            let item = object! as! AVPlayerItem;
            switch (item.status) {
            case AVPlayerItem.Status.failed:
                if (self.eventSink != nil) {
                    self.eventSink?(FlutterError(code: "VideoError", message: "Failed to load video", details: nil));
                }
                break;
            case AVPlayerItem.Status.unknown:
                break;
            case AVPlayerItem.Status.readyToPlay:
                print("Got keyPath \(keyPath) - AVPlayerItem.Status.readyToPlay");
                item.add(self.renderer.getVideoOutput());
                setupEventSinkIfReadyToPlay();
                updatePlayingState();
                break;
            default:
                break;
            }
        } else if (keyPath == "presentationSize" || keyPath == "duration") {
            let item = object as! AVPlayerItem;
            if (item.status == AVPlayerItem.Status.readyToPlay) {
                print("Got keyPath \(keyPath) - AVPlayerItem.Status.readyToPlay");
                // Due to an apparent bug, when the player item is ready, it still may not have determined
                // its presentation size or duration. When these properties are finally set, re-check if
                // all required properties and instantiate the event sink if it is not already set up.
                setupEventSinkIfReadyToPlay();
                updatePlayingState();
            }
        } else if (keyPath == "playbackLikelyToKeepUp") {
            if (self.player!.currentItem!.isPlaybackLikelyToKeepUp) {
                updatePlayingState();
                self.eventSink?(["event": "bufferingEnd"]);
            }
        } else if (keyPath == "playbackBufferEmpty") {
            self.eventSink?(["event": "bufferingStart"]);
        } else if (keyPath == "playbackBufferFull") {
            self.eventSink?(["event": "bufferingEnd"]);
        }
    }
    
    func radiansToDegrees(_ radians: Float) -> Float {
        // Input range [-pi, pi] or [-180, 180]
        let degrees = GLKMathRadiansToDegrees(radians);
        if (degrees < 0) {
            // Convert -90 to 270 and -180 to 180
            return degrees + 360;
        }
        // Output degrees in between [0, 360]
        return degrees;
    }
    
    func CMTimeToMillis(_ time: CMTime) -> Int64 {
        // When CMTIME_IS_INDEFINITE return a value that matches TIME_UNSET from ExoPlayer2 on Android.
        // Fixes https://github.com/flutter/flutter/issues/48670
        let TIME_UNSET: Int64 = -9223372036854775807;
        if (CMTIME_IS_INDEFINITE(time)) { return TIME_UNSET; }
        if (time.timescale == 0) { return 0; }
        return time.value * 1000 / Int64(time.timescale);
    }
    
    func rootViewController() -> UIViewController {
        // TODO: (hellohuanlin) Provide a non-deprecated codepath. See
        // https://github.com/flutter/flutter/issues/104117
        return UIApplication.shared.keyWindow!.rootViewController!;
    }
    
    func getVideoCompositionWithTransform(_ transform: CGAffineTransform, asset: AVAsset, videoTrack: AVAssetTrack) -> AVMutableVideoComposition {
        let instruction = AVMutableVideoCompositionInstruction();
        instruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: asset.duration);
        let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack);
        layerInstruction.setTransform(self.preferredTransform!, at: CMTime.zero);
        let videoComposition = AVMutableVideoComposition();
        instruction.layerInstructions = [layerInstruction];
        videoComposition.instructions = [instruction];
        
        // If in portrait mode, switch the width and height of the video
        var width = videoTrack.naturalSize.width;
        var height = videoTrack.naturalSize.height;
        let rotationDegrees: Int =
        Int(round(radiansToDegrees(Float(atan2(self.preferredTransform!.b, self.preferredTransform!.a)))));
        if (rotationDegrees == 90 || rotationDegrees == 270) {
            width = videoTrack.naturalSize.height;
            height = videoTrack.naturalSize.width;
        }
        videoComposition.renderSize = CGSizeMake(width, height);
        
        // TODO(@recastrodiaz): should we use videoTrack.nominalFrameRate ?
        // Currently set at a constant 30 FPS
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30);
        
        return videoComposition;
    }
    
    func createVideoOutputAndDisplayLink(_ frameUpdater: FrameUpdater) {
        self.displayLink = CADisplayLink(target: frameUpdater, selector: #selector(FrameUpdater.updateFrame));
        self.displayLink!.add(to: RunLoop.current, forMode: RunLoop.Mode.common);
        self.displayLink!.isPaused = true;

        print("Created video output");
    }
    
    func updatePlayingState() {
        if (!self.isInitialized) {
            return;
        }
        if (self.isPlaying) {
            self.player!.play();
        } else {
            self.player!.pause();
        }
        self.displayLink!.isPaused = !self.isPlaying;
    }
    
    func setupEventSinkIfReadyToPlay() {
        if (self.eventSink != nil && !self.isInitialized && self.player != nil) {
            if (self.player!.currentItem == nil || self.player!.status != AVPlayer.Status.readyToPlay) {
                return;
            }

            print("setupEventSinkIfReadyToPlay enter");

            let currentItem = self.player!.currentItem;
            let size = currentItem?.presentationSize;
            let width = size?.width;
            let height = size?.height;
            
            // Wait until tracks are loaded to check duration or if there are any videos.
            let asset = currentItem?.asset;
            if (asset?.statusOfValue(forKey: "tracks", error: nil) != AVKeyValueStatus.loaded) {
                let cmd = #function;
                func trackCompletionHandler() {
                    if (asset?.statusOfValue(forKey: "tracks", error: nil) != AVKeyValueStatus.loaded) {
                        // Cancelled, or something failed.
                        return;
                    }
                    // This completion block will run on an AVFoundation background queue.
                    // Hop back to the main thread to set up event sink.
                    self.performSelector(onMainThread: Selector(cmd), with: self, waitUntilDone: false);
                };
                asset?.loadValuesAsynchronously(forKeys: ["tracks"], completionHandler: trackCompletionHandler);
                return;
            }
            
            let videoTracks = asset?.tracks(withMediaType: AVMediaType.video);
            let hasVideoTracks = videoTracks != nil && videoTracks!.count != 0;
            let hasNoTracks = asset?.tracks.count == 0;
            
            // The player has not yet initialized when it has no size, unless it is an audio-only track.
            // HLS m3u8 video files never load any tracks, and are also not yet initialized until they have
            // a size.
            if ((hasVideoTracks || hasNoTracks) && height == CGSizeZero.height &&
                width == CGSizeZero.width) {
                return;
            }
            // The player may be initialized but still needs to determine the duration.
            let duration = duration();
            if (duration == 0) {
                return;
            }

            //Fix from https://github.com/flutter/flutter/issues/66413
            let track = videoTracks!.first;
            let naturalSize = track!.naturalSize;
            let prefTrans = track!.preferredTransform;
            let realSize = CGSizeApplyAffineTransform(naturalSize, prefTrans);
            let realW = abs(realSize.width);
            let realH = abs(realSize.height);
            let w = realW > width! ? realW : width!;
            let h = realH > height! ? realH : height!;
            self.renderer.updateTextureSize(width: w, height: h);

            print("Update video size: \(w) x \(h)");
            
            self.isInitialized = true;
            updatePlayingState();
            self.eventSink?([
                "event" : "initialized",
                "duration" : duration,
                "width" : w,
                "height" : h
            ]);
        }
    }

    public func getSharedTextureId() -> GLuint {
        return self.renderer.getTextureId();
    }
    
    public func play() {
        self.isPlaying = true;
        updatePlayingState();
    }
    
    public func pause() {
        self.isPlaying = false;
        updatePlayingState();
    }
    
    public func position() -> Int64 {
        return CMTimeToMillis(self.player!.currentTime());
    }
    
    public func duration() -> Int64 {
        // Note: https://openradar.appspot.com/radar?id=4968600712511488
        // `[AVPlayerItem duration]` can be `kCMTimeIndefinite`,
        // use `[[AVPlayerItem asset] duration]` instead.
        return CMTimeToMillis(self.player!.currentItem!.asset.duration as CMTime);
    }
    
    public func seekTo(location: Int64) {
        // TODO(stuartmorgan): Update this to use completionHandler: to only return
        // once the seek operation is complete once the Pigeon API is updated to a
        // version that handles async calls.
        self.player?.seek(to: CMTimeMake(value: location, timescale: 1000), toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero);
    }
    
    public func setIsLooping(isLooping: Bool) {
        self.isLooping = isLooping;
    }
    
    public func setVolume(volume: Float) {
        self.player?.volume = ((volume < 0.0) ? 0.0 : ((volume > 1.0) ? 1.0 : volume));
    }
    
    public func setPlaybackSpeed(speed: Float) {
        // See https://developer.apple.com/library/archive/qa/qa1772/_index.html for an explanation of
        // these checks.
        if (speed > 2.0 && !self.player!.currentItem!.canPlayFastForward) {
            if (self.eventSink != nil) {
                self.eventSink?(FlutterError(code: "VideoError", message: "Video cannot be fast-forwarded beyond 2.0x", details: nil));
            }
            return;
        }
        
        if (speed < 1.0 && !self.player!.currentItem!.canPlaySlowForward) {
            if (self.eventSink != nil) {
                self.eventSink!(FlutterError(code: "VideoError", message: "Video cannot be slow-forwarded", details:nil));
            }
            return;
        }
        
        self.player?.rate = speed;
    }
    
    // FlutterStreamHandler.onCancelWithArguments interface
    public func onCancel(withArguments arguments: Any?) -> FlutterError? {
        self.eventSink = nil;
        return nil;
    }
    
    // FlutterStreamHandler.onListenWithArguments interface
    public func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        self.eventSink = events;
        // TODO(@recastrodiaz): remove the line below when the race condition is resolved:
        // https://github.com/flutter/flutter/issues/21483
        // This line ensures the 'initialized' event is sent when the event
        // 'AVPlayerItemStatusReadyToPlay' fires before self.eventSink is set (this function
        // onListenWithArguments is called)
        setupEventSinkIfReadyToPlay();
        return nil;
    }
    
    /// This method allows you to dispose without touching the event channel.  This
    /// is useful for the case where the Engine is in the process of deconstruction
    /// so the channel is going to die or is already dead.
    func disposeSansEventChannel() {
        self.disposed = true;
        self.playerLayer!.removeFromSuperlayer();
        self.displayLink!.invalidate();
        let currentItem = self.player!.currentItem;
        currentItem?.removeObserver(self, forKeyPath: "status");
        currentItem?.removeObserver(self, forKeyPath: "loadedTimeRanges");
        currentItem?.removeObserver(self, forKeyPath: "presentationSize");
        currentItem?.removeObserver(self, forKeyPath: "duration");
        currentItem?.removeObserver(self, forKeyPath: "playbackLikelyToKeepUp");
        currentItem?.removeObserver(self, forKeyPath: "playbackBufferEmpty");
        currentItem?.removeObserver(self, forKeyPath: "playbackBufferFull");
        
        self.player!.replaceCurrentItem(with: nil);
        NotificationCenter.default.removeObserver(self);
    }
    
    func dispose() {
        disposeSansEventChannel();
        if (self.eventChannel != nil) {
            self.eventChannel!.setStreamHandler(nil);
        }
        self.renderer.dispose();
    }
}
