import Flutter
import AVFoundation
import OpenGLES.ES3
import GLKit
import three3d_egl

public class VideoRender: NSObject, FlutterTexture {
    var fboTargetPixelBuffer: CVPixelBuffer? = nil;
    var fboTextureCache: CVOpenGLESTextureCache? = nil;
    var fboTexture: CVOpenGLESTexture? = nil;
    var fboId: GLuint = 0;
    var rboId: GLuint = 0;

    var videoTextureCache: CVOpenGLESTextureCache? = nil;
    
    var glWidth: Double = 640;
    var glHeight: Double = 480;
    
    var eglEnv: EglEnv?;
    var shareEglCtx: EAGLContext?;
    
    var worker: VideoRenderWorker? = nil;
    
    var videoOutput: AVPlayerItemVideoOutput? = nil;
    
    var disposed: Bool = false;

    init(_ dummy: Int) {
        print("Video render initializing");
        
        let pixBuffAttributes: [String : Any] = [
            (kCVPixelBufferPixelFormatTypeKey as String): kCVPixelFormatType_32BGRA,
            (kCVPixelBufferOpenGLCompatibilityKey as String): kCFBooleanTrue!,
            (kCVPixelBufferOpenGLESCompatibilityKey as String): kCFBooleanTrue!,
            (kCVPixelBufferIOSurfacePropertiesKey as String): [:]
        ];
        videoOutput = AVPlayerItemVideoOutput(pixelBufferAttributes: pixBuffAttributes);

        super.init();

        self.setup();
    }
    
    func setup() {
        initEGL();
        self.worker = VideoRenderWorker();
        self.worker!.setup();
    }
    
    func getEgl() -> Array<Int64> {
        var _egls = [Int64](repeating: 0, count: 6);
        _egls[2] = self.eglEnv!.getContext();
        return _egls;
    }
    
    public func getVideoOutput() -> AVPlayerItemVideoOutput {
        return videoOutput!;
    }
    
    public func getTextureId() -> GLuint {
        return CVOpenGLESTextureGetName(fboTexture!);
    }
    
    public func updateTextureSize(width: Double, height: Double) {
        glWidth = width;
        glHeight = height;

        let res = self.createCVBufferWithSize(
            size: CGSize(width: glWidth, height: glHeight),
            context: self.eglEnv!.context!
        );
        if (!res) { return; }

        glBindTexture(CVOpenGLESTextureGetTarget(fboTexture!), CVOpenGLESTextureGetName(fboTexture!));
        //glTexImage2D(GLenum(GL_TEXTURE_2D), 0, GL_RGBA8, glWidth, glHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, null);
        //glBindTexture(GLenum(GL_TEXTURE_2D), 0);

        glViewport(0, 0, GLsizei(glWidth), GLsizei(glHeight));

        glBindRenderbuffer(GLenum(GL_RENDERBUFFER), rboId);
        glRenderbufferStorage(GLenum(GL_RENDERBUFFER), GLenum(GL_DEPTH24_STENCIL8), GLsizei(glWidth), GLsizei(glHeight));
        glBindRenderbuffer(GLenum(GL_RENDERBUFFER), 0);

        print("FlutterVideoPlayer: Frame buffer new size: \(glWidth) x \(glHeight) fboTexture=\(fboTexture)");

        glBindFramebuffer(GLenum(GL_FRAMEBUFFER), fboId);
        glFramebufferTexture2D(GLenum(GL_FRAMEBUFFER), GLenum(GL_COLOR_ATTACHMENT0), GLenum(GL_TEXTURE_2D), CVOpenGLESTextureGetName(fboTexture!), 0);
        let status = glCheckFramebufferStatus(GLenum(GL_FRAMEBUFFER));
        if (status != GL_FRAMEBUFFER_COMPLETE) {
            print("updateTextureSize: abnormal frame buffer status: \(status)");
        }
        glBindFramebuffer(GLenum(GL_FRAMEBUFFER), 0);
    }

    public func copyPixelBuffer() -> Unmanaged<CVPixelBuffer>? {
        print("Texture widget got new frame!");
        if (videoOutput == nil) {
            return nil;
        }
        let outputItemTime = videoOutput!.itemTime(forHostTime: CACurrentMediaTime());
        if (videoOutput!.hasNewPixelBuffer(forItemTime: outputItemTime)) {
            let pixBuf = videoOutput!.copyPixelBuffer(forItemTime: outputItemTime, itemTimeForDisplay: nil);
            return pixBuf != nil ? Unmanaged.passRetained(pixBuf!) : nil;
        } else {
            return nil;
        }
    }

    public func updateVideoTexture() {
        if (self.eglEnv == nil) { return; }
        
        self.eglEnv!.makeCurrent();

        //print("updateVideoTexturez: Got new frame!");
        if (videoOutput == nil) {
            return;
        }
        let outputItemTime = videoOutput!.itemTime(forHostTime: CACurrentMediaTime());
        if (videoOutput!.hasNewPixelBuffer(forItemTime: outputItemTime)) {
            let pixBuf = videoOutput!.copyPixelBuffer(forItemTime: outputItemTime, itemTimeForDisplay: nil);
            if (pixBuf != nil) {
                var videoTexture: CVOpenGLESTexture? = nil;
                let ret = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                                       videoTextureCache!,
                                                                       pixBuf!,
                                                                       nil,
                                                                       GLenum(GL_TEXTURE_2D),
                                                                       GL_RGBA,
                                                                       GLsizei(glWidth),
                                                                       GLsizei(glHeight),
                                                                       GLenum(GL_BGRA),
                                                                       GLenum(GL_UNSIGNED_BYTE),
                                                                       0,
                                                                       &videoTexture);
                if (ret != kCVReturnSuccess) {
                    NSLog("Failed to CVOpenGLESTextureCacheCreateTextureFromImage %d", ret);
                    return;
                }

                let videoTexId = CVOpenGLESTextureGetName(videoTexture!);
                updateTexture(videoTexId);
            }
        }
    }

    func updateTexture(_ sourceTexture: GLuint) -> Bool {
        //glEnable(GLenum(GL_BLEND));
        //glBlendFunc(GLenum(GL_SRC_ALPHA), GLenum(GL_ONE_MINUS_SRC_ALPHA));

        glViewport(0, 0, GLsizei(glWidth), GLsizei(glHeight));
        
        glBindFramebuffer(GLenum(GL_FRAMEBUFFER), fboId);
        glClear(GLbitfield(GL_COLOR_BUFFER_BIT) | GLbitfield(GL_DEPTH_BUFFER_BIT) | GLbitfield(GL_STENCIL_BUFFER_BIT));
        self.worker!.renderTexture(texture: sourceTexture, matrix: nil, isFBO: false);
        glBindFramebuffer(GLenum(GL_FRAMEBUFFER), 0);
        glFinish();

        //self.eglEnv!.swapBuffers();
        
        // TODO: callback to flutter notifying texture updated
        // flutter side can do texture copy immediately to its local context
        // then use the local texture copy for further rendering
        
        return true;
    }
    
    // ==================================
    func initEGL() {
        self.shareEglCtx = ThreeEgl.getContext(key: 3);
        
        print("VideoRenderer: Shared GL context is \(String(describing: self.shareEglCtx)), thread=\(Thread.current)");
        
        self.eglEnv = EglEnv();
        self.eglEnv!.setupRender(shareContext: self.shareEglCtx);
        self.eglEnv!.makeCurrent();

        print("FlutterVideoPlayer: Frame buffer init size: \(glWidth) x \(glHeight)");

        glEnable(GLenum(GL_BLEND));
        glBlendFunc(GLenum(GL_ONE), GLenum(GL_ONE_MINUS_SRC_ALPHA));
        glEnable(GLenum(GL_CULL_FACE));
        
        initTextureCacheBuffers();
        initOffscreenTargetTexture();
    }

    func initTextureCacheBuffers() {
        var ret = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, nil, self.eglEnv!.context!, nil, &fboTextureCache);
        if (ret != kCVReturnSuccess) {
            NSLog("initVideoFrameBuffer: Failed to CVOpenGLESTextureCacheCreate %d for frame buffer", ret);
            return;
        }
        ret = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, nil, self.eglEnv!.context!, nil, &videoTextureCache);
        if (ret != kCVReturnSuccess) {
            NSLog("initVideoFrameBuffer: Failed to CVOpenGLESTextureCacheCreate %d for video texture", ret);
            return;
        }
    }

    func initOffscreenTargetTexture() {
        let res = self.createCVBufferWithSize(
            size: CGSize(width: glWidth, height: glHeight),
            context: self.eglEnv!.context!
        );
        if (!res) { return; }

        glBindTexture(CVOpenGLESTextureGetTarget(fboTexture!), CVOpenGLESTextureGetName(fboTexture!));

        glViewport(0, 0, GLsizei(glWidth), GLsizei(glHeight));

        // create a normal (no MSAA) FBO to hold a render-to-texture
        glGenFramebuffers(1, &fboId);
        glBindFramebuffer(GLenum(GL_FRAMEBUFFER), fboId);

        glGenRenderbuffers(1, &rboId);
        glBindRenderbuffer(GLenum(GL_RENDERBUFFER), rboId);
        glRenderbufferStorage(GLenum(GL_RENDERBUFFER), GLenum(GL_DEPTH24_STENCIL8), GLsizei(glWidth), GLsizei(glHeight));
        glBindRenderbuffer(GLenum(GL_RENDERBUFFER), 0);

        // attach a texture to FBO color attachement point
        glFramebufferTexture2D(GLenum(GL_FRAMEBUFFER), GLenum(GL_COLOR_ATTACHMENT0), GLenum(GL_TEXTURE_2D), CVOpenGLESTextureGetName(fboTexture!), 0);

        // attach a rbo to FBO depth attachement point
        glFramebufferRenderbuffer(GLenum(GL_FRAMEBUFFER), GLenum(GL_DEPTH_ATTACHMENT), GLenum(GL_RENDERBUFFER), rboId);
        glFramebufferRenderbuffer(GLenum(GL_FRAMEBUFFER), GLenum(GL_STENCIL_ATTACHMENT), GLenum(GL_RENDERBUFFER), rboId);

        // check FBO status
        let status = glCheckFramebufferStatus(GLenum(GL_FRAMEBUFFER));
        if (status != GL_FRAMEBUFFER_COMPLETE) {
            return; //error
        }
    }
    
    func createCVBufferWithSize(size: CGSize, context: EAGLContext) -> Bool {
        let res = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, nil, context, nil, &fboTextureCache);
        if (res != kCVReturnSuccess) {
            NSLog("Failed to CVOpenGLESTextureCacheCreate %d", res);
            return false;
        }
        
        let attrs = [
            kCVPixelBufferPixelFormatTypeKey: NSNumber(value: kCVPixelFormatType_32BGRA),
            kCVPixelBufferOpenGLCompatibilityKey: kCFBooleanTrue!,
            kCVPixelBufferOpenGLESCompatibilityKey: kCFBooleanTrue!,
            kCVPixelBufferIOSurfacePropertiesKey: [:]
        ] as CFDictionary;
        
        let cv2 = CVPixelBufferCreate(kCFAllocatorDefault, Int(size.width), Int(size.height),
                                      kCVPixelFormatType_32BGRA, attrs, &fboTargetPixelBuffer);
        if (cv2 != kCVReturnSuccess) {
            NSLog("Failed to CVPixelBufferCreate %d", cv2);
            return false;
        }
        
        let cvr = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                               fboTextureCache!,
                                                               fboTargetPixelBuffer!,
                                                               nil,
                                                               GLenum(GL_TEXTURE_2D),
                                                               GL_RGBA,
                                                               GLsizei(size.width),
                                                               GLsizei(size.height),
                                                               GLenum(GL_BGRA),
                                                               GLenum(GL_UNSIGNED_BYTE),
                                                               0,
                                                               &fboTexture);
        if (cvr != kCVReturnSuccess) {
            NSLog("Failed to CVOpenGLESTextureCacheCreateTextureFromImage %d", cvr);
            return false;
        }
        
        return true;
    }
    
    func checkGlError(op: String) {
        let error = glGetError();
        if (error != GL_NO_ERROR) {
            print("ES30_ERROR", "\(op): glError \(error)")
        }
    }
    
    public func dispose() {
        self.disposed = true;
        
        self.shareEglCtx = nil;
        
        self.eglEnv!.dispose();
        self.eglEnv = nil;
        
        EAGLContext.setCurrent(nil);
    }
    
}
